class SmsTasks < Volt::Task

  def onboard_user(phone, carrier)
    address = SMSEasy::Client.sms_address(phone, carrier)
    mail = SendGrid::Mail.new do |m|
      m.to = address.to_s
      m.from = 'notify@splatoon.polymetric.xyz'
      m.subject = 'Splatoon/Notify'
      m.text = "Welcome to Splatoon/Notify! Updates are sent at 0100, 0500, 0900, 1300, 1800, and 2100 hours, central time."
    end
    res = Grid.send(mail)
    puts res.code
    puts res.body
  end

  def send_sms
    store._players.each.then do |player|
      address = SMSEasy::Client.sms_address(player._phone, player._carrier)
      mail = SendGrid::Mail new do |m|
        m.to = address
        m.from = 'notify@splatoon.polymetric.xyz'
        m.subject = 'Splatoon/Notify'
        m.text = "Splatoon/Notify!\nTurf War:\n#{MapTasks.printmap(0).sync}\n#{MapTasks.printmap(1).sync}\n#{MapTasks.printmode}:\n#{MapTasks.printmap(2).sync}\n#{MapTasks.printmap(3).sync}"
      end
      res = Grid.send(mail)
      puts res.code
      puts res.body
    end
  end

end
